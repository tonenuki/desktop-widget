# Небольшой пример по работе с e-ink дисплеем на 7 цветов

---

## Описание параметров

* `--mode` возможные значения `slideshow`, `picture` и `clear`, режимы работы примера.
* `--colormode` возможные значения `color` и `black`, указывает необходимо ли включать цвета при отрисовке.
* `--clear` возможные значения `True` и `False`, указывае необходимо ли очистить дисплей перед отрисовкой нового изображения.
* `--path` путь до папки с файлами или путь до файла.
* `--contrast` значение контраста, рекомендуется использовать `20`.
* `--color` значение цвета, рекомендуется использовать `2`.
* `--rotation` угол поворота изображения, для портретного режима `90` для горизонтального `0`.

## Данный пример расчитан на работу на raspberrypi 

## Фото дисплея

![pic1.jpg](doc/images/pic1.jpg)

![pic2.jpg](doc/images/pic2.jpg)

![pic3.jpg](doc/images/pic3.jpg)

## Подключение

| Контакт на модуле | Контакт на raspberrypi |
| ----------------- | ---------------------- |
| VCC               | 3.3v или 5v            |
| GND               | GND                    |
| DIN               | 19                     |
| CLK               | 23                     |
| CS                | 24                     |
| DC                | 22                     |
| RST               | 11                     |
| BUSY              | 18                     |

Рабочее напряжение зависит от версии дисплея

## Лицензия Apache

```
Copyright 2021 Iosif Sidelnikov(Tonenuki)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
